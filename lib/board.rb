class Board
  attr_reader :grid

  def initialize(grid = Array.new(3) { Array.new(3) })
    @grid = grid
  end

  def place_mark(pos, mark)
    @grid[pos[0]][pos[1]] = mark
  end

  def empty?(pos)
    @grid[pos[0]][pos[1]].nil?
  end

  def get_moves
    ind = (0..grid.length - 1).to_a
    moves = []
    ind.each { |i| ind.each { |j| moves << [i, j] if empty?([i, j]) } }
    moves
  end

  def get_wins
    ind = (0..grid.length - 1).to_a
    wins = []
    wins << ind.map { |i| [i, i] } # left diag
    wins << ind.map { |i| [i, grid.length - i - 1] } # right diag
    ind.each do |i|
      wins << ind.map { |j| [i, j] } # row i
      wins << ind.map { |j| [j, i] } # column i
    end
    wins
  end

  def winner
    get_wins.each do |win|
      marks = win.map { |pos| grid[pos[0]][pos[1]] }
      return marks[0] if !marks[0].nil? && marks.uniq.length == 1
    end
    nil
  end

  def over?
    !winner.nil? || @grid.none? { |row| row.any?(&:nil?) }
  end
end
