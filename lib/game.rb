require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :board

  def initialize(player_one, player_two)
    @players = [player_one, player_two]
    @current_player_num = 0
    @board = Board.new
  end

  def current_player
    @players[@current_player_num]
  end

  def switch_players!
    @current_player_num = 1 - @current_player_num
  end

  def play_turn
    @board.place_mark(current_player.get_move, current_player.mark)
    switch_players! unless @board.over?
  end
end
