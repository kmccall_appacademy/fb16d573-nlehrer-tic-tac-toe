class HumanPlayer
  attr_reader :board, :name, :mark
  attr_writer :mark

  def initialize(name)
    @name = name
    @mark = :X
  end

  def display(board)
    board.grid.each do |row|
      puts row.map { |col| col.nil? ? '_' : col }.join(' ')
    end
  end

  def get_move
    print 'Go where? '
    input = gets.chomp
    [input[1].to_i, input[3].to_i]
  end
end
