class ComputerPlayer
  attr_reader :board, :name, :mark
  attr_writer :mark

  def initialize(name)
    @name = name
    @mark = :O
  end

  def display(board)
    @board = board
  end

  def get_move
    moves = @board.get_moves
    moves.each do |move|
      fake_board = Board.new(@board.grid.map(&:dup))
      fake_board.place_mark(move, @mark)
      return move if fake_board.winner == @mark
    end
    moves.sample
  end
end
