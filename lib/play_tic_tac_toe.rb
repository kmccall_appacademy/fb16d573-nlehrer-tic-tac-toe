require_relative 'game'
require_relative 'human_player'
require_relative 'computer_player'

print 'What is your name? '
name = gets.chomp
player_one = HumanPlayer.new(name)
player_two = ComputerPlayer.new('Comp')

game = Game.new(player_one, player_two)
until game.board.over?
  game.current_player.display(game.board)
  game.play_turn
end
player_one.display(game.board)
puts game.board.winner.nil? ? 'Draw!' : "#{game.current_player.name} wins!"
